
$(document).ready(function () {
    $('#tableKategori').dataTable();
    /* attach a submit handler to the form */
    $("#editKategori").submit(function(event) {
        /* stop form from submitting normally */
        event.preventDefault();
        var idKategori = $('[name=id_kategori]').val();
        var nmKategori = $('[name=nama_kategori]').val();

        console.log(idKategori+','+nmKategori);
        var url = "http://localhost/ekage/kategori/update_data_kategori";
        $.ajax({
            url: url,
            type: 'POST',
            dataType : 'json',
            //masukan parameter
            data: {
                id_kategori: idKategori,
                nama_kategori: nmKategori
            },
            success: function (data) {
                //menampilkan data di console log
                console.log("masuk");
                console.log(data);
                swal({
                    position: 'center',
                    type: 'success',
                    title: 'Data Kategori Berhasil Diubah !',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function () {
                    window.location.replace("http://localhost/ekage/kategori");

                },1500);

            },
            error: function (data) {
                console.log(data);
                swal({
                    position: 'center',
                    type: 'error',
                    title: 'Data Kategori Gagal Diubah !',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });


    });
    $("#form-tambahKategori").submit(function(event) {
        /* stop form from submitting normally */
        event.preventDefault();

        var nmKategori = $('[name=nama_kategori]').val();
        var url = "http://localhost/ekage/kategori/insert_kategori";
        $.ajax({
            url: url,
            type: 'POST',
            dataType : 'json',
            //masukan parameter
            data: {
                nama_kategori: nmKategori,
            },
            success: function (data) {
                //menampilkan data di console log
                console.log("masuk");
                console.log(data);
                if (data.message == "Duplicate"){
                    swal({
                        position: 'center',
                        type: 'error',
                        title: 'Data Kategori Sudah Ada !',
                        showConfirmButton: false,
                        timer: 1500
                    });
                }else{
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Data Kategori Berhasil Ditambahkan !',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setTimeout(function () {
                        window.location.replace("http://localhost/ekage/kategori");

                    },1500);
                }
            },
            error: function (data) {
                console.log(data);
                swal({
                    position: 'center',
                    type: 'error',
                    title: 'Data Kategori Gagal Ditambahkan !',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        })
    });
    $(".deleteKategori").click(function () {
        var idKategori = $(this).data('idkategori');
        var nmKategori = $(this).data('namakategori');
        swal({
            title: 'anda yakin ingin menghapus '+nmKategori+'?',
            text: "Data yang dihapus tidak dapat dikembalikan !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus data!',
            cancelButtonText: 'Batal !',
        }).then((result) => {
            if (result.value) {
            var url = "http://localhost/ekage/kategori/hapus_kategori";
            $.ajax({
                url: url,
                type: 'POST',
                dataType : 'json',
                //masukan parameter
                data: {
                    id_kategori : idKategori
                },
                success: function (data) {
                    //menampilkan data di console log
                    console.log("masuk");
                    console.log(data);
                    swal({
                        position: 'center',
                        type: 'success',
                        title: 'Data Kategori Berhasil Dihapus !',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setTimeout(function () {
                        window.location.replace("http://localhost/ekage/kategori");

                    },1500);

                },
                error: function (data) {
                    console.log(data);
                    swal({
                        position: 'center',
                        type: 'error',
                        title: 'Data Kategori Gagal Dihapus !',
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            });
        }
    })

    });

});
