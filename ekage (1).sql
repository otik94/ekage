-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2018 at 10:04 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ekage`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_barang`
--

CREATE TABLE IF NOT EXISTS `mst_barang` (
  `id_barang` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(50) NOT NULL,
  `nm_barang` varchar(200) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_barang`),
  KEY `id_kategori` (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mst_barang`
--

INSERT INTO `mst_barang` (`id_barang`, `kode_barang`, `nm_barang`, `harga_beli`, `harga_jual`, `stok`, `id_kategori`, `status`) VALUES
(1, '1', 'abc', 1000, 1200, 10, 1, 0),
(2, '0002', 'Pisang', 500, 1000, 1000, 2, 1),
(3, 'MR-001', 'kriuk', 300, 500, 100, 3, 1),
(4, '3', 'pensil', 700, 1000, 10, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_kategori`
--

CREATE TABLE IF NOT EXISTS `mst_kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nm_kategori` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `mst_kategori`
--

INSERT INTO `mst_kategori` (`id_kategori`, `nm_kategori`, `status`) VALUES
(1, 'cair', 1),
(2, 'Obat-obatan', 1),
(3, 'Minuman', 1),
(4, 'Makanan Pokok', 1),
(5, 'Makanan Ringan', 1),
(6, 'ATK', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_roles`
--

CREATE TABLE IF NOT EXISTS `mst_roles` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mst_roles`
--

INSERT INTO `mst_roles` (`id_role`, `role_name`) VALUES
(1, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `mst_user`
--

CREATE TABLE IF NOT EXISTS `mst_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `roles` int(1) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mst_user`
--

INSERT INTO `mst_user` (`id_user`, `username`, `password`, `roles`, `last_login`, `status`) VALUES
(1, 'admin', 'ec5c48d23b01dbc5e12c52d501f686a2', 1, '2018-02-23 03:08:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_transaksi`
--

CREATE TABLE IF NOT EXISTS `tb_detail_transaksi` (
  `id_detail_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` int(15) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `kode_barang` varchar(50) DEFAULT NULL,
  `nm_barang` varchar(200) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `status` enum('0','1') NOT NULL COMMENT '1= sudah diproses, 0 belum diproses',
  PRIMARY KEY (`id_detail_transaksi`),
  UNIQUE KEY `id_detail_transaksi` (`id_detail_transaksi`),
  KEY `id_transaksi` (`id_transaksi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `tb_detail_transaksi`
--

INSERT INTO `tb_detail_transaksi` (`id_detail_transaksi`, `id_transaksi`, `id_barang`, `kode_barang`, `nm_barang`, `harga_jual`, `jumlah`, `total`, `status`) VALUES
(30, 2, 2, '0002', 'Pisang', 1000, 3, NULL, '1'),
(32, 3, 2, '0002', 'Pisang', 1000, 1, NULL, '1'),
(37, 0, 2, '0002', 'Pisang', 1000, 3, NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi`
--

CREATE TABLE IF NOT EXISTS `tb_transaksi` (
  `id_transaksi` int(15) NOT NULL AUTO_INCREMENT,
  `tgl_transaksi` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `total_transaksi` int(11) DEFAULT NULL,
  `bayar` int(11) DEFAULT NULL,
  `kembalian` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`id_transaksi`, `tgl_transaksi`, `total_transaksi`, `bayar`, `kembalian`) VALUES
(1, '2018-03-07 17:00:00', NULL, NULL, NULL),
(2, '2018-03-08 17:00:00', NULL, NULL, NULL),
(3, '2018-03-08 17:00:00', NULL, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mst_barang`
--
ALTER TABLE `mst_barang`
  ADD CONSTRAINT `mst_barang_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `mst_kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
