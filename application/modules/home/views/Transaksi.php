
<body class="home-page">
  <div class="container ">
    <center><?php echo $this->session->flashdata('msg');?></center>
    <h1>E-Kage</h1>
    <section class="content">
      <div class="row">
        <div class="box box-info">

          <!-- /.box-header -->
          <!-- form start -->
          <!-- <?php echo form_open('Home', array('class'=>'form-horizontal')); ?> -->
          <form action="<?php echo base_url('addTransaksiTabel')?>" class="form-horizontal" method="post">
            <div class="box-body">
              <div class="form-group">
              <label for="No Transaksi" class="col-sm-2 control-label box-title"><font  size="5" color="white">No. Transaksi:</font></label>
              <div class="col-xs-3">
              <input class="form-control" type="text" id="noTransaksi" name="noTransaksi" placeholder="Nomor Transaksi" readonly>
            </div>
            <label for="Total" class="col-sm-2 control-label box-title"><font  size="5" color="white">Total Harga:</font></label>
            <div class="col-xs-3">
            <input type="number" class="form-control" id="total" placeholder="Total Harga" readonly>
          </div>
        </div>

        <br><br>
        <div class="form-group">
          <!-- <label for="Kode" class="col-sm-2 control-label box-title">Kode Barang</label> -->
          <label for="Kode" class="col-sm-2 control-label box-title"><font  size="4" color="white">Nama Barang</font></label>
          <div class="col-xs-3">
            <!-- <input type="text" class="form-control" name="kode_brg" id="kode_brg" placeholder="Kode barang"> -->
            <input type="text" class="form-control" name="barang" id="kode_brg" placeholder="Nama barang">
          </div>

          <!-- <div id="detail_barang" "style=position:absolute;"> -->
          <!-- </div> -->
          <!-- <label for="Jumlah" class="col-sm-2 control-label box-title"><font  size="4" color="white">Jumlah</font></label>
          <div class="col-xs-3">
            <input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Jumlah">
          </div> -->
          <label for="Jumlah" class="col-sm-2 control-label box-title"><font  size="4" color="white">Jumlah</font></label>
          <div class="col-xs-3">
            <input type="text" class="form-control" name="nama_barang" id="jumlah" placeholder="Jumlah">
          </div>
          <!-- <?php
          // error_reporting(0);
          // $b=$brg->row_array();
          // ?>
          <input type="text" name="nabar" value="<?php echo $b['nm_barang'];?>" style="width:380px;margin-right:5px;" class="form-control input-sm" readonly> -->
          <!-- </div> -->
        </div>
        <!-- /.box-body -->

        <!-- /.box-footer -->
        <div class="box-footer">
          <button type="submit" name="submit"class="btn btn-info pull-right">Add</button>
        </div>
        <br>
        <br>
        <br>
        <br>
      </form>
      <datalist id="barang">
        <?php foreach ($barang->result()as $b) {
          # code...
          echo "<option value='$b->nm_barang'>";
        } ?>
      </datalist>
      <table class="table table-bordered table-condensed" style="font-size:15px;margin-top:10px;">
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Barang</th>
            <th>Nama Barang</th>
            <th style="text-align:center;">Jumlah</th>
            <th style="text-align:center;">Harga(Rp)</th>
            <th style="text-align:center;">Sub Total(Rp)</th>
            <th style="width:100px;text-align:center;">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; $total=0; foreach ($detail as $r){ ?>
            <tr class="gradeU">
              <td><?php echo $no ?></td>
              <td><?php echo $r->kode_barang?></td>
              <td><?php echo $r->nm_barang?></td>
              <!-- <td><?php echo $r->nm_barang.' - '.anchor('Home/hapusitem/'.$r->id_detail_transaksi,'Hapus',array('style'=>'color:red;')) ?></td> -->
              <td><?php echo $r->jumlah ?></td>
              <td>Rp. <?php echo number_format($r->harga_jual,2) ?></td>
              <td>Rp. <?php echo number_format($r->jumlah*$r->harga_jual,2) ?></td>
              <!-- <td style="text-align:center;"><a href="<?php echo base_url().'admin/penjualan/remove/'.$r->id_detail_transaksi;?>" class="btn btn-warning btn-xs"><span class="fa fa-close"></span> Batal</a></td> -->
              <td style="text-align:center;"><a href="<?php echo base_url('Home/hapusitem/'.$r->id_detail_transaksi);?>" class="btn btn-warning btn-xs"><span class="fa fa-close"></span> Batal</a></td>
            </tr>
            <?php $total=$total+($r->jumlah*$r->harga_jual);
            $no++; } ?>
            <tr class="gradeA">
              <td colspan="4">T O T A L</td>
              <td>Rp. <?php echo number_format($total,2);?></td>
            </tr>

          </tbody>
        </table>
        <form action="" method="post">
          <div class="box-body">
            <div class="form-group">
              <label for="Bayar" class="col-sm-2 control-label box-title"><font  size="4" color="white">Bayar</font></label>
              <div class="col-sm-2">
                <input type="number" class="form-control" id="bayar" placeholder="Bayar">
              </div>
            </div>
            <div class="form-group">
              <label for="Kembali" class="col-sm-2 control-label box-title"><font  size="4" color="white">Kembali</font></label>
              <div class="col-sm-2">
                <input type="number" class="form-control" id="kembali" placeholder="Kembali">
              </div>
            </div>

            <div class="box-footer pull-right">
              <button type="submit" class="btn btn-danger">Batal</button> | <?php echo anchor('Home/selesai_belanja','Selesai',array('class'=>'btn btn-info btn-sm'))?>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- <script type="text/javascript">
  document.getElementById("inlineFormInputGroupKodeBarang").value = "";
</script> -->
<script type="text/javascript">
// $(document).ready(function(){
//   //Ajax kabupaten/kota insert
//   $("#kode_brg").focus();
//   $("#kode_brg").on("input",function(){
//     var kobar = {kode_brg:$(this).val()};
//     var url = "http://localhost/ekage/Home/transaksi";
//     $.ajax({
//       type: "POST",
//       url : url,
//       data: kobar,
//       success: function(data){
//         console.log("masuk");
//         console.log(data);
//       }
//     });
//   });
//
//   // $("#kode_brg").keypress(function(e){
//   //   if(e.which==13){
//   //     $("#jumlah").focus();
//   //   }
//   // });
// });
// $(document).ready(function () {
//
//
//   $('.btnHapus').click(function(e){
//     var id = $(this).attr('name');
//     swal({
//       title : "Apakah anda ingin menghapus ?",
//       type : "info",
//       showCancelButton : true,
//       CloseOnConfirm : false,
//       showLoaderOnConfirm : true,
//     },function(){
//
//       $.ajax({
//         url : '<?php echo site_url('CBackendAlatUkur/del/') ?>'+id,
//
//         success : function(data){
//           if(data == 1){
//             swal({
//               title : "data berhasil di hapus",
//               type : "success",
//               showConfirmButton: false,
//             },
//             window.setTimeout(function(){
//               location.href = "<?php site_url('CBackendAlatUkur/index')?>"
//             },1000))
//           }
//         }
//       });
//     }
//   );
//
// });
// });
</script>
