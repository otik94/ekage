<?php
/**
 * Created by PhpStorm.
 * User: trihut
 * Date: 27/12/2017
 * Time: 14:55
 */

class M_transaksi extends CI_Model
{
  function __construct(){
    $this->load->database();
    $this->table = 'mst_barang';

  }
  function search($q)
    {
      $this->db->select('id_barang, kode_barang ,nm_barang');
      $this->db->like('kode_barang',$q);
      $this->db->or_like('nm_barang',$q);
      $this->db->order_by('id_barang','desc');
      return $this->db->get('mst_barang');

    }
    function get_allBarang($table,$orderby,$where=null,$id=null){
      if ($where != null && $id != null) {
        $this->db->where($where,$id);
      }
      $this->db->select('*');
      $this->db->from($table);
      $this->db->order_by($orderby,'desc');

      return  $this->db->get();
    }
    // function get_barang($search){
    //     $this->db->select('*');
    //     $this->db->from('mst_barang b');
    //     $this->db->join('mst_kategori k', 'b.id_kategori=k.id_kategori');
    //     $this->db->where('b.status', '1');
    //     if (!empty($search)) {
    //         $this->db->group_start();
    //         $this->db->like('b.kode_barang', $search);
    //         $this->db->or_like('b.nm_barang', $search);
    //         $this->db->group_end();
    //
    //     }
    //
    //     $query = $this->db->get();
    //     if ($query->num_rows() != 0) {
    //         return $query->result();
    //     } else {
    //         return false;
    //     }
    // }
  function tampil_detail_transaksi(){
    $query = "SELECT td.id_detail_transaksi,td.kode_barang,td.jumlah,td.harga_jual,b.nm_barang
              FROM tb_detail_transaksi as td ,mst_barang as b
              where b.id_barang=td.id_barang and td.status='0'";
    return $this->db->query($query);
  }
  function simpan_barang()
    {
        $nama_barang      =  $this->input->post('barang');
        $jumlah         =  $this->input->post('jumlah');
        // $nama_barangs         =  $this->input->post('nama_barang');
        // $total         =  $this->input->post('total');
        // $nama_barang         =  $this->input->post('nama_barang');
        $idbarang       = $this->db->get_where('mst_barang',array('nm_barang'=>$nama_barang))->row_array();
        $data           = array('id_barang'=>$idbarang['id_barang'],
                                'jumlah'=>$jumlah,
                                'harga_jual'=>$idbarang['harga_jual'],
                                'nm_barang'=>$idbarang['nm_barang'],
                                'kode_barang'=>$idbarang['kode_barang'],
                                // 'total'=>$idbarang['total'],
                                'status'=>'0');
        $this->db->insert('tb_detail_transaksi',$data);
    }

    function hapusitem($id)
    {
        $this->db->where('id_detail_transaksi',$id);
        $this->db->delete('tb_detail_transaksi');
    }


    function selesai_belanja($data)
    {
        $this->db->insert('tb_transaksi',$data);
        $last_id=  $this->db->query("select id_transaksi from tb_transaksi order by id_transaksi desc")->row_array();
        $this->db->query("update tb_detail_transaksi set id_transaksi='".$last_id['id_transaksi']."' where status='0'");
        $this->db->query("update tb_detail_transaksi set status='1' where status='0'");
    }

}
