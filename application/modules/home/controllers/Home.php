<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: trihut
 * Date: 19/12/2017
 * Time: 11:00
 */
class Home extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_home');
        $this->load->model('m_transaksi');
    }

    public function index()
    {
        $header['pageTitle'] = "Login | E-kage";
        $this->basecontroller->loadviews('home', $header);
    }

    public function cekHarga()
    {
        $header['pageTitle'] = "Cek Harga | E-kage";
        $footer['menu'] = "cekHarga";
        if ($this->input->post('cari') != null) {
            $cari = $this->input->post('cari');
            $data['cari'] = $cari;
            $data['data_barang'] = $this->m_home->get_barang($cari);
            $this->basecontroller->loadviews('cekharga', $header, $data, $footer);
        }else{
            $this->basecontroller->loadviews('cekharga', $header, null, $footer);

        }
    }

    public function login()
    {
        $header['pageTitle'] = "Login Page | E-kage";
        $footer['menu'] = "login";
        $this->basecontroller->loadviews('login', $header, null, $footer);
    }

    public function logout()
    {
        $this->basecontroller->logout();
    }
// ======================================= transaksi ===================
    public function transaksi(){
      if(isset($_POST['submit'])){
        $this->m_transaksi->simpan_barang();
        redirect('transaksi');
      }else{
        $header['pageTitle'] = "Transaksi Page | E-kage";
        $footer['menu'] = "Transaksi";
        $kobar=$this->input->post('kode_brg');
        // $data['brg']=$this->m_home->get_barangs($kobar);
        $data['barang']=$this->m_home->get_barangs();
        $data['detail']=$this->m_transaksi->tampil_detail_transaksi()->result();
        $this->basecontroller->loadviews('transaksi',$header, $data, $footer);
      }

    }
    // function get_barang(){
    //
  	// 	$kobar=$this->input->post('kode_brg');
  	// 	$x['brg']=$this->m_home->get_barangs($kobar);
  	// 	$this->basecontroller->loadviews('DetailBarangJual', $x);
    //
  	// }
    public function search(){
  		$q = @$_GET['q'];
  		$barang = $this->m_transaksi->search($q)->result();
  		echo json_encode($barang);
  	}
    function getDataBarang($id){
  // $data['data'] = $this->m_transaksi->get_barang($cari);
      $data['data'] = $this->m_transaksi->get_allBarang('mst_barang','id_barang','id_barang',$id)->result();
  		echo json_encode($data['data']);
  	}
    function hapusitem()
   {
       $id=  $this->uri->segment(3);
       $this->m_transaksi->hapusitem($id);
       // echo "1";
       redirect('transaksi');
   }


   function selesai_belanja()
   {
       $tanggal=date('Y-m-d');
       // $user=  $this->session->userdata('username');
       // $id_op=  $this->db->get_where('operator',array('username'=>$user))->row_array();
       $data=array('tgl_transaksi'=>$tanggal);
       $this->m_transaksi->selesai_belanja($data);
       redirect('transaksi');
   }

}
