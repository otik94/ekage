<?php
/**
 * Created by PhpStorm.
 * User: Tri Hutama
 * Date: 22/01/2018
 * Time: 08:13
 */

class M_kategori extends CI_Model
{
    // function get_barang($search = null)
    // {
    //     $this->db->select('*');
    //     $this->db->from('mst_barang b');
    //     $this->db->join('mst_kategori k', 'b.id_kategori=k.id_kategori');
    //     $this->db->where('b.status', '1');
    //     if (!empty($search)) {
    //         $this->db->group_start();
    //         $this->db->like('b.kode_barang', $search);
    //         $this->db->or_like('b.nm_barang', $search);
    //         $this->db->group_end();
    //
    //     }
    //
    //     $query = $this->db->get();
    //     if ($query->num_rows() != 0) {
    //         return $query->result();
    //     } else {
    //         return false;
    //     }
    // }

    function edit_kategori($search = 0)
    {
        $this->db->select('*');
        $this->db->from('mst_kategori b');
        // $this->db->join('mst_kategori k', 'b.id_kategori=k.id_kategori');

        if ($search != 0) {
            $this->db->where('b.id_kategori', $search);
        }
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function update_data_kategori($id_kategori, $data)
    {
        $this->db->where('id_kategori', $id_kategori);
        $this->db->update('mst_kategori', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function tambah_kategori($data)
    {
        $this->db->insert('mst_kategori', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function cek_duplikasi($nama_kategori)
    {
        $this->db->select('*');
        $this->db->from('mst_kategori b');
        $this->db->where('b.nm_kategori', $nama_kategori);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function hapus_kategori($id_kategori)
    {
        // $this->db->set('status','0');
        // $this->db->where('id_kategori', $id_kategori);
        // $this->db->update('mst_kategori');
        $this->db->delete('mst_kategori', array('id_kategori' => $id_kategori));
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
        // $this->db->delete('mst_kategori', array('id_kategori' => $id_kategori));
    		// return;

    }

    function get_kategori()
    {
        $this->db->select('*');
        $this->db->from('mst_kategori k');
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
