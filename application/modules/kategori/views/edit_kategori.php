
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-archive"></i> Kelola Data Barang
            <small>Edit Barang</small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->


                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Ubah Detail Barang</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" id="editKategori" action="<?php echo site_url('kategori/update_data_kategori') ?>" method="post">
                        <div class="box-body">
                            <?php if (!empty($data_kategori)){
                            foreach ($data_kategori

                            as $kategori):
                            ?>
                            <input type="hidden" name="id_kategori" value="<?php echo $kategori->id_kategori ?>">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Nama Kategori</label>
                                        <input type="text" class="form-control required email" id="nama_kategori"
                                               name="nama_kategori" maxlength="128" value="<?php echo $kategori->nm_kategori ?>">
                                    </div>
                                </div>
                            </div>
                            <?php
                            endforeach;
                            } ?>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-left">
                                <a href="<?php echo site_url('Kategori')?>" class="btn btn-warning btn-sm raised">Kembali</a>

                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9 text-right">
                                <input type="submit" class="btn btn-primary btn-sm raised" value="Submit"/>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
