<?php
/**
 * Created by PhpStorm.
 * User: Tri Hutama
 * Date: 20/01/2018
 * Time: 19:04
 */
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-archive"></i> Kelola Data Kategori Barang
            <small>Tambah Kategori Barang</small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->


                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Masukan Detail Kategori Barang</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form role="form" id="form-tambahKategori" action="<?php echo site_url('kategori/insert_kategori') ?>" method="post"
                          role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="kategori">Kategori Barang</label>
                                        <input type="text" class="form-control required" id="nama_kategori"
                                               name="nama_kategori" maxlength="128">
                                    </div>

                                </div>
                            </div>


                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <div class="col-md-3 col-sm-3 col-xs-3 text-left">
                                <a href="<?php echo site_url('kategori')?>" class="btn btn-warning btn-sm raised">Kembali</a>

                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9 text-right">
                                <input type="submit" class="btn btn-primary btn-sm raised" value="Submit"/>
                                <input type="reset" class="btn btn-default btn-sm raised" value="Reset"/>

                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
