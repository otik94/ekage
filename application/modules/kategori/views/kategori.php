<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-archive"></i> Kategori Barang
            <small>Tambah, Edit, hapus</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-left">
                <div class="form-group">
                    <a class="btn btn-primary raised btn-sm" href="<?php echo site_url('tambahKategori') ?>"><i
                                class="fa fa-plus"></i> Tambah Kategori Barang</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Kategori Barang</h3>

                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table id="tableKategori" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Kategori</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($data_kategori)) {
                                $no = 0;
                                foreach ($data_kategori as $kategori) {
                                    ?>
                                    <tr>
                                        <td><?php echo ++$no ?></td>
                                        <td><?php echo $kategori->nm_kategori ?></td>
                                        <td class="text-center">
                                            <a class="btn btn-sm btn-info"
                                               href="<?php echo site_url('Kategori/edit_kategori/' . $kategori->id_kategori) ?>"><i
                                                        class="fa fa-edit"></i></a>
                                            <a class="btn btn-sm btn-danger deleteKategori" href="#"
                                               data-idkategori="<?php echo $kategori->id_kategori; ?>"
                                               data-namakategori="<?php echo $kategori->nm_kategori ?>"><i
                                                        class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>

                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();
            var link = jQuery(this).get(0).href;
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
