<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Created by PhpStorm.
* User: trihut
* Date: 19/12/2017
* Time: 11:00
*/
class Kategori extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->basecontroller->isLoggedIn();
    $this->load->model('m_kategori');
  }

  public function index()
  {

    $header['pageTitle'] = 'Kategori | E-kage';
    // if ($this->input->post('searchText') != null) {
    //     $cari = $this->input->post('searchText');
    //     $data['searchText'] = $cari;
    //     $data['data_barang'] = $this->m_barang->get_barang($cari);
    // } else {
    $data['data_kategori'] = $this->m_kategori->get_kategori();
    //
    // }
    $this->basecontroller->loadviews('Kategori', $header, $data);
  }

  public function tambah_kategori()
  {
    $header['pageTitle'] = 'Tambah kategori | E-kage';
    $data['kategori'] = $this->m_kategori->get_kategori();

    $this->basecontroller->loadviews('tambah_kategori', $header, $data);

  }

  public function insert_kategori()
  {
    $this->load->library('form_validation');

    $this->form_validation->set_rules('nama_kategori', 'nama_kategori', 'required');



    if ($this->form_validation->run() == FALSE) {
      //            $this->index();
      echo json_encode();
    } else {

      $nama_kategori = $this->input->post('nama_kategori');


      $kategori = array(
        'nm_kategori' => $nama_kategori

      );

      if ($this->m_kategori->cek_duplikasi($nama_kategori)) {
        $json = array(
          "status" => "200",
          "message" => "Duplicate"
        );
        echo json_encode($json);
      } else {
        $result = $this->m_kategori->tambah_kategori($kategori);
        if ($result) {
          $json = array(
            "status" => "200",
            "message" => "Success"
          );
          echo json_encode($json);
        }
      }
    }
  }

  public function edit_kategori($id_kategori)
  {
    $header['pageTitle'] = 'Edit Kategori | E-kage';
    $data['data_kategori'] = $this->m_kategori->edit_kategori($id_kategori);
    $data['kategori'] = $this->m_kategori->get_kategori();
    $this->basecontroller->loadviews('edit_kategori', $header, $data);

  }

  public function update_data_kategori()
  {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('id_kategori', 'id_kategori', 'required');

    $this->form_validation->set_rules('nama_kategori', 'nama_kategori', 'required');



    if ($this->form_validation->run() == FALSE) {

    } else {
      $id_kategori = $this->input->post('id_kategori');
      $nama_kategori = $this->input->post('nama_kategori');

      $kategori = array(
        'nm_kategori' => $nama_kategori
      );
      $result = $this->m_kategori->update_data_kategori($id_kategori, $kategori);
      if ($result) {
        $json = array(
          "status" => "200",
          "message" => "Success"
        );
        echo json_encode($json);
      }
    }
  }

  public function hapus_kategori()
  {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('id_kategori', 'id_kategori', 'required');
    if ($this->form_validation->run() == FALSE) {

    } else {
      $id_kategori = $this->input->post('id_kategori');
      $hapus = $this->m_kategori->hapus_kategori($id_kategori);
      if ($hapus) {
        $json = array(
          "status" => "200",
          "message" => "Success"
        );
        echo json_encode($json);
      }
    }
  }

}
